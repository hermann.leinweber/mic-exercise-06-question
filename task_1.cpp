#include <iostream>
#include <mutex>
#include <functional>
#include <thread>
#include <chrono>

class Fancy_async_lock
{
    std::thread worker_thread;

    public:
    Fancy_async_lock(const std::function<int()> task_1){
	    std::cout << "Constructor: START" << std::endl;
        worker_thread = std::thread([task_1] {
            int result = task_1();
            std::cout << "Task 1 returned " << result << std::endl;
        });
	    std::cout << "Constructor: END" << std::endl;
    }

    ~Fancy_async_lock(){
        if ( worker_thread.joinable()){
            worker_thread.join();
        } // else joined allready
    }

    void run_when_ready(const std::function<int()> &task_2){
        std::cout << "run_when_ready: START" << std::endl;
        worker_thread.join();
	int result = task_2();
        std::cout << "Task 2 returned " << result << std::endl;
        std::cout << "run_when_ready: END" << std::endl;
    }
};

int do_work_1() {
    std::cout << "Task 1: START" << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(3));
    std::cout << "Task 1: END" << std::endl;
    return 1;
}

int do_work_2() {
    std::cout << "Task 2: START" << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(3));
    std::cout << "Task 2: END" << std::endl;
    return 2;
}

void serialize_it() {
    std::this_thread::sleep_for(std::chrono::seconds(4));
    std::cout << "Wait time before calling run" << std::endl;
}


int main(int argc, char const *argv[])
{
    Fancy_async_lock s = Fancy_async_lock(do_work_1);
    //serialize_it();
    s.run_when_ready(do_work_2);
    return 0;
}
