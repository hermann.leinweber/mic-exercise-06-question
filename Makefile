CXX=g++
CXXFLAGS=-g -std=c++20 -Wall -lpthread
BIN=prog

SRC=$(wildcard *.cpp)
OBJ=$(SRC:%.cpp=%.o)

all: $(OBJ)
	$(CXX) -o $(BIN) $(CXXFLAGS) $^

%.o: %.cpp
	$(CXX) $@ -c $<

clean:
	rm -f *.o
	rm $(BIN)

run: all
	./$(BIN)
