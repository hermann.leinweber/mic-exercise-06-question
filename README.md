# Frage zu Übung 6

Bei der Implementierung von Aufgabe 1 bin ich auf ein Problem gestoßen.

Leider kann ich den Fehler nicht finden. Hat jemand aus MIC eine Idee?

Die Diskussion kann hier in MRs oder im Moodle Forum gestartet werden.

Vielen Dank für Eure Hilfe

Hermann

## Problem

Es besteht eine mir nicht erklärbare race condition.

Aus einem mir unbekannten Grund wird `do_work_2` auch schon im Konstruktor aufgerufen.

```
$ make run
g++ task_1.o -c task_1.cpp
g++: warning: task_1.o: linker input file unused because linking not done
g++ -o prog -g -std=c++20 -Wall -lpthread task_1.o
./prog
Constructor: START
Constructor: END
run_when_ready: START
Task 2: START
Task 2: END
Task 1 returned 2
Task 2: START
Task 2: END
Task 2 returned 2
run_when_ready: END
```

Wenn man den zweiten Teil erst startet, wenn der thread fertig ist, funktioniert der Code wie erwartet.

```
# serialize_it einschalten l.58
$ make run
g++ task_1.o -c task_1.cpp
g++: warning: task_1.o: linker input file unused because linking not done
g++ -o prog -g -std=c++20 -Wall -lpthread task_1.o
./prog
Constructor: START
Constructor: END
Task 1: START
Task 1: END
Task 1 returned 1
Wait time before calling run
run_when_ready: START
Task 2: START
Task 2: END
Task 2 returned 2
run_when_ready: END
```

- Was passiert hier und warum wird die Reference auf die Funktion durch den frühen Aufruf von `run_when_ready` verändert?
